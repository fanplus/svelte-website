export async function get() {
  console.log('import.meta', import.meta)
  const imports = import.meta.glob("./blog/*.{md,svx}");
  let body = [];
  for (const path in imports) {
      body.push(
          imports[path]().then(({ metadata }) => {
              return {
                  metadata,
                  path,
              };
          })
      );
  }

  const posts = await Promise.all(body);
  console.log('posts', posts)

  return {
      body: JSON.stringify(posts)
  }
}