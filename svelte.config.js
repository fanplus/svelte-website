
// Imports
import sveltePreprocess from 'svelte-preprocess';
import { resolve } from 'path';
import { dirname } from 'path';
import { fileURLToPath } from 'url';
import { createRequire } from 'module';
import { mdsvex } from "mdsvex";
import adapter from '@sveltejs/adapter-static';
import { mdsvexConfig } from "./mdsvex.config.js";

// Custom require function as replacement for the require from the commonJS in ES Module
const customRequire = createRequire(import.meta.url); // jshint ignore:line

import WindiCSS from 'vite-plugin-windicss/dist/index.mjs'

// Custom __dirname as replacement for the __dirname from the commonJS in ES Module
const __dirname = dirname(fileURLToPath(import.meta.url)); // jshint ignore:line
const pkg = customRequire('./package.json');

console.log('mdsvexConfig', mdsvexConfig)
/** @type {import('@sveltejs/kit').Config} */
const config = {
	extensions: ['.svelte', ...mdsvexConfig.extensions],
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: [
		sveltePreprocess({
			scss: {
				// prependData: `@import 'src/lib/styles/variables.scss';`,
				outputStyle: 'compressed'
			}
		}),
		mdsvex(mdsvexConfig)
	],

	kit: {
		// hydrate the <div id="svelte"> element in src/app.html
		target: '#svelte',
		adapter: adapter(),
		vite: {
			define: {
        'process.env.VITE_BUILD_TIME': JSON.stringify(new Date().toISOString()),
      },
	    plugins: [
	      WindiCSS(),
	    ],
	    ssr: {
				// noExternal: [...Object.keys(pkg.dependencies || {})],
			}
		}
	},
};

export default config;
