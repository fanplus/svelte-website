module.exports = {
    mode: 'jit',
    purge: {
        content: ['./src/**/*.{html,js,svelte,ts}'],
    },
    theme: {
        extend: {},
    },
    variants: {
        extend: {
            listStyleType: ['hover', 'focus'],
            listStylePosition: ['hover', 'focus'],
        },
    },
    plugins: [
        require('windicss/plugin/forms'),
        require('windicss/plugin/aspect-ratio'),
        require('windicss/plugin/line-clamp'),
        require('windicss/plugin/filters'),
        require('windicss/plugin/scroll-snap'),     
    ],
}